<%@page import="java.util.List"%>
<%@page import="Entities.Task"%>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>MalerSiden</title>
        <meta name="generator" content="WYSIWYG Web Builder 12 - http://www.wysiwygwebbuilder.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="1Årsprojekt.css" rel="stylesheet">
        <link href="loggedMaler.css" rel="stylesheet">
        <script>
            /*Dont judge, jeg er ikke god til det her javascript og kunne ikke f� arrayet til at virke*/
            function hideUser() {
                var showUser = document.getElementById("UserBlock");
                var showTask = document.getElementById("TaskManagerBox");
                var showProduct = document.getElementById("ProductManager");
                if (showUser.style.display === "none") {
                    showUser.style.display = "block";
                    showTask.style.display = "none";
                    showProduct.style.display = "none"
                } else {
                    showUser.style.display = "none";
                }
            }
            function hideTask() {
                var showUser = document.getElementById("UserBlock");
                var showTask = document.getElementById("TaskManagerBox");
                var showProduct = document.getElementById("ProductManager");
                if (showTask.style.display === "none") {
                    showTask.style.display = "block";
                    showUser.style.display = "none";
                    showProduct.style.display = "none";
                } else {
                    showTask.style.display = "none";
                }
            }

            function hideProduct() {
                var showProduct = document.getElementById("ProductManager");
                var showUser = document.getElementById("UserBlock");
                var showTask = document.getElementById("TaskManagerBox");
                if (showProduct.style.display === "none") {
                    showProduct.style.display = "block";
                    showTask.style.display = "none";
                    showUser.style.display = "none";
                } else {
                    showProduct.style.display = "none";
                }
            }
            function popUp(url) {
                UserWindow = window.open(url, 'ProductAssignmentPage', 'width = 1200, height = 900');
                return false;
            }
        </script>
    </head>
    <body>
        <%
            if (session.getAttribute("username") == null) {
                System.out.println("LoggedMaler: SessionId: " + session.getId());
                System.out.println("LoggedMaler: Sessionusername: " + session.getAttribute("username"));
                session.invalidate();
                request.setAttribute("errormessage", "Invalid. You need to login properly!");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
        %>
        <div id="Layer2" style="position:relative;text-align:center;width:100%;height:437px;float:left;clear:left;display:block;z-index:4;">
            <div id="Layer2_Container" style="width:970px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
                <img src="images/img0021.png" id="MergedObject1" alt="" title="" style="border-width:0;position:absolute;left:35px;top:0px;width:900px;height:85px;z-index:0">
                <div id="wb_Heading1" style="position:absolute;left:310px;top:93px;width:350px;height:79px;z-index:1;">
                    <h1 id="Heading1">Velkommen maler<br></h1></div>
                <div id="wb_Text1" style="position:absolute;left:310px;top:156px;width:246px;height:16px;z-index:2;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">Se listen over dine potentiele kunder.</span></div>
            </div>
        </div>
    </div>

</div>
<div id="wb_LayoutGrid1">
    <div id="LayoutGrid1">
        <div class="row">
            <div id="split">
                <div id="RightBox" class="TaskBox">
                    <button class="button" onclick="hideUser()">H�ndter brugere</button>
                    <button class="button" onclick="hideTask()">H�ndter Opgaver</button>
                    <button class="button" onclick="hideProduct()">H�ndter Produkter!(Udvalg)</button>
                    <div id="TaskManagerBox">
                        <div id="AdminBox" class="DeleteBox">
                            <h2>Slet Opgave:</h2>
                            <form action="TaskManager" method="post">
                                <p>${dMessage}</p>
                                <input type="hidden" name="type" value="delete">
                                <label id="req">Opgave Id:</label>
                                <input type="text" name="delete" placeholder="fx. 23" required>
                                <input type="submit" value="Delete Task" name="delete">
                            </form>
                        </div>
                        <div id="AdminBox" class="UpdateBox">
                            <h2>Opdater Opgave:</h2>
                            <form action="TaskManager" method="post">
                                <p>${uMessage}</p>
                                <input type="hidden" name="type" value="update">
                                <label id="req">Opgave id:</label>
                                <input type="text" name="assignmentId" placeholder="fx. 331" >
                                <label>Dato Oprettet:</label>
                                <input type="text" name="upDateCr" placeholder="2017-10-24" >
                                <label>Date For Udf�rsel:</label>
                                <input type="text" name="upDateJob" placeholder="2017-11-27" >
                                <label>Total Pris:</label>
                                <input type="text" name="upTotal" placeholder="999.95" >
                                <label>Kunde nr:</label>
                                <input type="text" name="upCustommer" placeholder="fx. 45">
                                <input type="submit" value="Update Task" name="update">
                            </form>
                        </div>
                        <div id="AdminBox" class="CreateBox"> 
                            <h2>Opret Opgave</h2>
                            <form action="TaskManager" method="post">
                                <p>${cMessage}</p>
                                <input type="hidden" name="type" value="create" required>
                                <label>Dato Oprettet(Blank = Dags dato):</label>
                                <input type="text" name="createDateCr" placeholder="fx. 2017-11-23">
                                <label>Dato For udf�rsel(Blank = om 4 uger):</label>
                                <input type="text" name="createDateJob" placeholder="fx. 2017-11-23">
                                <label id="req">Total Pris:</label>
                                <input type="text" name="createPriceTotal" placeholder="fx. 1299.55" required>
                                <label id="req">Kunde nr:</label>
                                <input type="text" name="createCustommerId" placeholder="fx. 23" required>
                                <input type="submit" value="Opret Opgave!" name="create">
                            </form>
                        </div>
                        <div id="AdminBox" class="CreateBox">
                            <h2>Tilknyt en maler</h2>
                            <form action="TaskManager" method="post">
                                <p>${aMessage}</p>
                                <input type="hidden" name="type" value="assign" required>
                                <label id="req">Opgave Id:</label>
                                <input type="text" name="TskId" placeholder="fx. 129" required>
                                <label id="req">Maler Id:</label>
                                <input type="text" name="painterId" placeholder="fx. 23" required>
                                <input type="submit" value="Tilknyt maler!" name="assign">
                            </form>
                        </div>
                        <div id="AdminBox" class="DeleteBox">
                            <h2>Fjern en maler</h2>
                            <form action="TaskManager" method="post">
                                <p>${aMessage}</p>
                                <input type="hidden" name="type" value="remove" required>
                                <label id="req">Opgave Id:</label>
                                <input type="text" name="TskId" placeholder="fx. 129" required>
                                <label id="req">Maler Id:</label>
                                <input type="text" name="painterId" placeholder="fx. 23" required>
                                <input type="submit" value="Fjern maler!" name="remove">
                            </form>
                        </div>       
                        <hr>
                        <div id="AdminBox" class="CreateBox">
                            <h2>Opret Produkt til Opgave</h2>
                            <form action="ProductAssignmentManager" method="post">
                                <input type="hidden" name="type" value="create" required>
                                <label id="req">Opgave Id:</label>
                                <input type="text" name="assignmentId" placeholder="id '3'">
                                <label id="req">Produkt Navn:</label>
                                <input type="text" name="productName" placeholder="wall">
                                <label id="req">Malingstype:</label>
                                <input id type="text" name="paintName" placeholder="Glans 1">
                                <label id="req">M�ngde:</label>
                                <input type="text" name="productAmount" placeholder="23">
                                <input type="submit" name="create" value="Opret Produkt">
                            </form>
                        </div>
                        <div id="AdminBox" class="UpdateBox">
                            <h2>Opdater Produkt for en Opgave</h2>
                            <form action="ProductAssignmentManager" method="post">
                                <input type="hidden" name="type" value="update" required>
                                <label id="req">Opgave Id:</label>
                                <input type="text" name="assignmentId" placeholder="id '3'">
                                <label id="req">Produkt Navn:</label>
                                <input type="text" name="productName" placeholder="'normalDoor' eller 'wall' etc">
                                <label id="req">Malingstype:</label>
                                <input id type="text" name="paintName" placeholder="Glans 1">
                                <label>M�ngde:</label>
                                <input type="text" name="productAmount" placeholder="23">
                                <input type="submit" name="update" value="Opdater Produkt">
                            </form>
                        </div>
                        <div id="AdminBox" class="DeleteBox">
                            <h2>Fjern et produkt fra en opgave</h2>
                            <form action="ProductAssignmentManager" method="post">
                                <input type="hidden" name="type" value="delete" required>
                                <label id="req">Opgave Id:</label>
                                <input type="text" name="assignmentId" placeholder="id '3'">
                                <label id="req">Produkt Navn:</label>
                                <input type="text" name="productName" placeholder="product 'Glans 5'">
                                <input type="submit" name="delete" value="Slet Produkt">
                            </form>
                        </div>
                    </div>
                    <div id="UserBlock">
                        <div id="AdminBox" class="DeleteBox">
                            <h2>Slet Bruger</h2>
                            <form action="UserManager" method="post">
                                <input type="hidden" name="type" value="delete">
                                <label id="req">Bruger id:</label>
                                <input type="text" name="delete" placeholder="fx. 213" required>
                                <input type="submit" value="Slet Bruger" name="delete">
                            </form>
                        </div>


                        <div id="AdminBox" class="UpdateBox">
                            <h2>Opdater Bruger</h2>
                            <form action="UserManager" method="post">
                                <input type="hidden" name="type" value="update">
                                <label id="req">Bruger id:</label>
                                <input type="text" name="dUserId" placeholder="fx. 213">
                                <label>Fornavn:</label>
                                <input type="text" name="uFirst" placeholder="">
                                <label>Efternavn</label>
                                <input type="text" name="uLast" placeholder="">
                                <label>Password:</label>
                                <input type="password" name="uPass" placeholder="">
                                <label>E-mail:</label>
                                <input type="text" name="uEmail" placeholder="">
                                <label>Telefon nr:</label>
                                <input type="text" name="uPhone" placeholder="">
                                <label>Role:(kunde/maler)</label>
                                <input type="text" name="uRole" placeholder="">
                                <label>Addresse:</label>
                                <input type="text" name="uAddress" placeholder="">
                                <label>Postnummer:</label>
                                <input type="text" name="uZip" placeholder="">
                                <label>By:</label>
                                <input type="text" name="uCity" placeholder="">
                                <input type="submit" name="update" value="Opdater Bruger!">
                            </form>
                        </div>
                        <div id="AdminBox" class="CreateBox">
                            <h2>Opret en ny Bruger</h2>
                            <form action="UserManager" method="post">
                                <input type="hidden" name="type" value="create">
                                <label id="req">Fornavn:</label>
                                <input type="text" name="uFirst" placeholder="Morten">
                                <label id="req">Efternavn:</label>
                                <input type="text" name="uLast" placeholder="Hansen">
                                <label>Password:(Hvis tom = random)</label>
                                <input type="password" name="uPass" placeholder="">
                                <label id="req">E-mail:</label>
                                <input type="text" name="uEmail" placeholder="">
                                <label id="req">Telefon nr:</label>
                                <input type="text" name="uPhone" placeholder="">
                                <label>Role:(tom = kunde)</label>
                                <input type="text" name="uRole" placeholder="">
                                <label id="req">Addresse:</label>
                                <input type="text" name="uAddress" placeholder="">
                                <label id="req">Postnummer:</label>
                                <input type="text" name="uZip" placeholder="">
                                <label>By:</label>
                                <input type="text" name="uCity" placeholder="">
                                <input type="submit" name="create" value="Opret Bruger!">
                            </form>
                        </div>
                        <button class="button" href="#" onclick="popUp('ViewUsersServlet')">Se bruger liste</button>
                    </div>
                    <div id="ProductManager">
                        <div id="AdminBox" class="DeleteBox">
                            <h2>Slet Maling!</h2>
                            <form action="ProductServlet" method="post">
                                <p>${dMessage}</p>
                                <input type="hidden" name="type" value="delete">
                                <label id="req">Produkt Id:</label>
                                <input type="text" name="id" placeholder="fx. 23" required>
                                <br>
                                <input type="submit" value="Delete Product" name="delete">
                            </form>
                        </div>
                        <div id="AdminBox" class="UpdateBox">
                            <h2>Opdater Maling!</h2>
                            <form action="ProductServlet" method="post">
                                <p>${uMessage}</p>
                                <input type="hidden" name="type" value="update">
                                <label id="req">Produkt ID:</label>
                                <input type="text" name="id" required>
                                <label id="req">Maling Navn:</label>
                                <input type='text' name='paintName' placeholder="glans 20" required>
                                <label for='ceil'>Loftmaling?</label>
                                <input id='ceil' type="checkbox" name="ceil" value='ceil'>
                                <label for='wall'>V�gmaling?</label>
                                <input id='wall' type="checkbox" name="wall" value='wall'>
                                <label for='door'>D�rmaling?</label>
                                <input id='door' type="checkbox" name="door" value='door'>
                                <label for='window'>Vinduesmaling?</label>
                                <input id='window' type="checkbox" name="window" value='window'>
                                <br>
                                <input type="submit" value="Opdater Maling!" name="update">
                            </form>
                        </div>
                        <div id="AdminBox" class="CreateBox"> 
                            <h2>Opret Maling!</h2>
                            <form action="ProductServlet" method="post">
                                <p>${cMessage}</p>
                                <input type="hidden" name="type" value="create" required>
                                <label>Maling Navn:</label>
                                <input type="text" name="name" placeholder="Glans 2">
                                <label for='ceil'>Loftmaling?</label>
                                <input id='ceil' type="checkbox" name="ceil" value='ceil'>
                                <label for='wall'>V�gmaling?</label>
                                <input id='wall' type="checkbox" name="wall" value='wall'>
                                <label for='door'>D�rmaling?</label>
                                <input id='door' type="checkbox" name="door" value='door'>
                                <label for='window'>Vinduesmaling?</label>
                                <input id='window' type="checkbox" name="window" value='window'>
                                <br>
                                <input type="submit" value="Opret Maling!" name="create">
                            </form>
                        </div>
                    </div>
                </div>
                <div id="LeftBox" class="ListBox">
                    <table id="tasktable">
                        <th>Opgave Id</th>
                        <th>Dato Oprettet</th>
                        <th>Dato for Udf�rsel</th>
                        <th>Estimeret pris</th>
                        <th>Kunde Id</th>
                            <%
                                try {
                                    List<Task> list = (List<Task>) request.getAttribute("tasklist");
                                    for (Task tasks : list) {
                                        //onclick=\"window.location='task?id=" + tasks.getTaskId() + "';\"
                                        out.println("<tr onclick=\"popUp('/WebApplication2/GetFullProductServlet?id=" + tasks.getTaskId() + "')\">");
                                        out.println("<td>" + tasks.getTaskId() + "</td>");
                                        out.println("<td>" + tasks.getDateOfCreation() + "</td>");
                                        out.println("<td>" + tasks.getDateOfJob() + "</td>");
                                        out.println("<td>" + tasks.getTotalPrice() + "</td>");
                                        out.println("<td>" + tasks.getCustommerId() + "</td>");
                                        out.println("</tr>");
                                    }
                                } catch (Exception e) {
                                }
                            %>
                    </table>
                </div>
            </div>

            <div class="col-1">
                <!--<textarea name="TextArea1" id="TextArea1" style="display:block;width:100%;height:574px;z-index:3;" rows="34" cols="152" spellcheck="false"></textarea>-->
                <!-- Er der en mening med dette felt?-->
            </div>
        </div>
    </div>

    <form method='get' action='Logout'>
        <button type='submit' name='logout' value='Logout' class='button'>
            Logout
        </button>
    </form>
</div>
</body>
</html>