<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="database.connection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.Enumeration"%>
<%@page import="CRUD.display"%>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgaver</title>
        <meta name="generator" content="WYSIWYG Web Builder 12 - http://www.wysiwygwebbuilder.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="1�rsprojekt.css" rel="stylesheet">
        <link href="Opgaver.css" rel="stylesheet">
        <script src="jquery-1.12.4.min.js"></script>
        <script>


            function ValidateOpret(theForm)
            {
                var regexp;
                regexp = /^[-+]?\d*\.?\d*$/;
                if (!regexp.test(theForm.Editbox1.value))
                {
                    alert("Please enter only digit characters in the \"Editbox1\" field.");
                    theForm.Editbox1.focus();
                    return false;
                }
                if (theForm.Editbox1.value.length < 5)
                {
                    alert("Please enter at least 5 characters in the \"Editbox1\" field.");
                    theForm.Editbox1.focus();
                    return false;
                }
                regexp = /^[-+]?\d*\.?\d*$/;
                if (!regexp.test(theForm.Editbox2.value))
                {
                    alert("Please enter only digit characters in the \"Editbox2\" field.");
                    theForm.Editbox2.focus();
                    return false;
                }
                return true;
            }

            function saveLocalStorage() {
                var var0 = document.getElementById("Editbox0");
                var var1 = document.getElementById("Editbox1");
                var var2 = document.getElementById("Editbox2");
                var var3 = document.getElementById("Editbox3");
                var var4 = document.getElementById("Editbox4");
                var var5 = document.getElementById("Editbox5");
                var var6 = document.getElementById("Editbox6");
                var var7 = document.getElementById("Editbox7");

                localStorage.setItem("test0", var0.value);
                localStorage.setItem("test1", var1.value);
                localStorage.setItem("test2", var2.value);
                localStorage.setItem("test3", var3.value);
                localStorage.setItem("test4", var4.value);
                localStorage.setItem("test5", var5.value);
                localStorage.setItem("test6", var6.value);
                localStorage.setItem("test7", var7.value);

            }
        </script>
        <script>
            $(document).ready(function ()
            {

                var $activeExtension1 = $('#Extension1 li.active');
                $('#Extension1 li a').hover(function ()
                {
                    $('#Extension1 li').removeClass('active');
                    $(this).closest('li').addClass('active');
                }, function ()
                {
                    $('#Extension1 li').removeClass('active');
                    $activeExtension1.addClass('active');
                });
            });
        </script>
    </head>
    <body>
        <%
            Connection conn;
            connection DB = new connection();
            conn = DB.openConnection();
            display disp = new display(conn);
            ArrayList<String> resultsetDoor = new ArrayList<>();
            resultsetDoor = disp.displayPaintDoor();
            ArrayList<String> resultsetWall = new ArrayList<>();
            resultsetWall = disp.displayPaintWall();
            ArrayList<String> resultsetCeil = new ArrayList<>();
            resultsetCeil = disp.displayPaintCeiling();
            ArrayList<String> resultsetWindow = new ArrayList<>();
            resultsetWindow = disp.displayPaintWindows();
            double total = 0;

            if (request.getAttribute("totalPrice") != null) {
                total = (Double) request.getAttribute("totalPrice");
            } else {
                total = 0;
            }


        %>
        <div id="wb_LayoutGrid1">
            <div id="LayoutGrid1">
                <div class="row">
                    <div class="col-1">
                        <div id="wb_Extension1" style="display:inline-block;width:100%;z-index:0;">
                            <ul id="Extension1">
                                <li><a href="./index.jsp">Hjem</a></li>
                                <li><a href="./Opgaver.jsp">Opgaver</a></li>
                                <li><a href="./Om_os.jsp">Om os</a></li>
                                <li><a href="./Support_&_login.jsp">Support & log in</a></li>
                                <li class="box"></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Layer1" style="position:relative;text-align:center;width:100%;height:437px;float:left;clear:left;display:block;z-index:47;">
            <div id="Layer1_Container" style="width:1300px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
                <div id="wb_Text10" style="position:absolute;left:950px;top:89px;width:250px;height:240px;z-index:1;">
                    <span style="color:#FFFFFF;font-family:Arial;font-size:13px;">Sammens�t selv dit tilbud og v�re med til at regulere prisen. Vi hj�lper dig at se den rette vej, og du f�r ingen overraskende fakturaer. Du kan blive oplyst om 70 procent af prisen. <br><br>N�r prisen er beregnet, skal du trykke opret, og du f�r tildelt et opgaveID som du kan bruge til reference hos maleren, hvis du m�tte have sp�rgsm�l. <br><br>Derefter kan du i kalenderen, booke tid til at malere kan komme ud og kigge p� opgaven. Her fasts�ttes ogs� endelig pris for hele arbejdet.</span></div>
            </div>
        </div>
        <div id="Layer2" style="position:relative;text-align:center;width:100%;height:437px;float:left;clear:left;display:block;z-index:48;">
            <div id="Layer2_Container" style="width:1300px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">

                <div id="wb_Text2" style="position:absolute;left:900px;top:48px;width:250px;height:16px;z-index:19;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">Antal af kvadratmeter som skal udf�res. </span></div>
                <div id="wb_Text2" style="position:absolute;left:900px;top:77px;width:250px;height:16px;z-index:19;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">Antal af kvadratmeter som skal udf�res. </span></div>
                <div id="wb_Text3" style="position:absolute;left:900px;top:104px;width:250px;height:16px;z-index:20;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">V�lg mellem 1, 2, 3.</span></div>
                <div id="wb_Text1" style="position:absolute;left:900px;top:135px;width:250px;height:16px;z-index:21;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">V�lg mellem 4, 5, 6.</span></div>
                <div id="wb_Text5" style="position:absolute;left:900px;top:167px;width:250px;height:16px;z-index:22;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">Antal af d�re der skal males</span></div>
                <div id="wb_Text6" style="position:absolute;left:900px;top:197px;width:250px;height:16px;z-index:23;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">V�lg mellem standard 1, 2, 3</span></div>
                <div id="wb_Text7" style="position:absolute;left:900px;top:228px;width:250px;height:16px;z-index:24;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">Antal af vinduer der skal males</span></div>
                <div id="wb_Text8" style="position:absolute;left:900px;top:259px;width:250px;height:16px;z-index:25;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;">V�lg mellem standard 1, 2, 3</span></div>
                <div id="wb_Text9" style="position:absolute;left:900px;top:294px;width:384px;height:16px;z-index:26;">
                    <span style="color:#800000;font-family:Arial;font-size:13px;"><strong><em>Timeprisen og endelig pris fasts�ttes efter bes�g af maleren.</em></strong></span></div>
                <div id="wb_Form1" style="position:absolute;left:494px;top:57px;width:365px;height:277px;z-index:27;">
                    <form name="Opret" method="POST" action="calclulateServlet" id="Form1" onsubmit="saveLocalStorage()">
                        <label for="Editbox1" id="Label1" style="position:absolute;left:10px;top:-16px;width:117px;height:16px;line-height:16px;z-index:2;">V�g</label>
                        <input type="text" id="Editbox1" style="position:absolute;left:282px;top:-16px;width:105px;height:16px;line-height:16px;z-index:3;" name="Editbox1" value="0" spellcheck="false">
                        <label for="Editbox0" id="Label0" style="position:absolute;left:10px;top:15px;width:117px;height:16px;line-height:16px;z-index:2;">Loft</label>
                        <input type="text" id="Editbox0" style="position:absolute;left:282px;top:15px;width:105px;height:16px;line-height:16px;z-index:3;" name="Editbox0" value="0" spellcheck="false">
                        <label for="Editbox2" id="Label2" style="position:absolute;left:10px;top:46px;width:117px;height:16px;line-height:16px;z-index:4;">Maling til v�g</label>
                        <select id="Editbox2" style="position:absolute;left:282px;top:46px;width:115px;height:26px;line-height:16px;z-index:5;" name="Editbox2">
                            <%  for (int q = 0; q < resultsetWall.size(); q++) {%>
                            <option><%= resultsetWall.get(q)%></option>
                            <% }%>
                        </select>
                        <label for="Editbox3" id="Label3" style="position:absolute;left:10px;top:77px;width:117px;height:16px;line-height:16px;z-index:6;">Maling til loft</label>
                        <select id="Editbox3" style="position:absolute;left:282px;top:77px;width:115px;height:26px;line-height:16px;z-index:7;" name="Editbox3">
                            <%  for (int q = 0; q < resultsetCeil.size(); q++) {%>
                            <option><%= resultsetCeil.get(q)%></option>
                            <% }%>
                        </select>
                        <label for="Editbox4" id="Label4" style="position:absolute;left:10px;top:108px;width:117px;height:16px;line-height:16px;z-index:8;">D�re</label>
                        <input type="text" id="Editbox4" style="position:absolute;left:282px;top:108px;width:105px;height:16px;line-height:16px;z-index:9;" name="Editbox4" value="0" spellcheck="false">
                        <label for="Editbox5" id="Label5" style="position:absolute;left:10px;top:139px;width:117px;height:16px;line-height:16px;z-index:10;">Maling til d�re</label>
                        <select id="Editbox5" style="position:absolute;left:282px;top:139px;width:115px;height:26px;line-height:16px;z-index:11;" name="Editbox5">
                            <%  for (int q = 0; q < resultsetDoor.size(); q++) {%>
                            <option><%= resultsetDoor.get(q)%></option>
                            <% }%>
                        </select>
                        <label for="Editbox6" id="Label6" style="position:absolute;left:10px;top:170px;width:117px;height:16px;line-height:16px;z-index:12;">Vinduer</label>
                        <input type="text" id="Editbox6" style="position:absolute;left:282px;top:170px;width:105px;height:16px;line-height:16px;z-index:13;" name="Editbox6" value="0" spellcheck="false">
                        <label for="Editbox7" id="Label7" style="position:absolute;left:10px;top:201px;width:117px;height:16px;line-height:16px;z-index:14;">Maling til vinduer</label>
                        <select id="Editbox7" style="position:absolute;left:282px;top:201px;width:115px;height:26px;line-height:16px;z-index:15;" name="Editbox7">
                            <%  for (int q = 0; q < resultsetWindow.size(); q++) {%>
                            <option><%= resultsetWindow.get(q)%></option>
                            <% }%>
                        </select>
                        <label for="Editbox8" id="Label8" style="position:absolute;left:10px;top:232px;width:156px;height:16px;line-height:16px;z-index:16;">Pris i alt uden moms</label>
                        <input type="text" id="Editbox8" style="position:absolute;left:233px;top:232px;width:102px;height:16px;line-height:16px;z-index:17;" name="Editbox8" value="<%=total%>" spellcheck="false">
                        <input type="submit" id="Button1" name="calc" value="Beregn" style="position:absolute;left:444px;top:287px;width:96px;height:25px;z-index:29;">
                        <input type="submit" id="Button1" name="create" value="Opret" style="position:absolute;left:294px;top:287px;width:96px;height:25px;z-index:29;">

                    </form>
                </div>
                <div id="wb_Text4" style="position:absolute;left:105px;top:187px;width:294px;height:160px;z-index:28;">
                    <span style="color:#800000;font-family:Arial;font-size:13px;">Malermaster Ejendomsservice. <br><br>Telefon : 12 34 56 78<br>Email : Malermester@mail.dk<br><br>Adresse : Adressevej 10. 1234 by</span><span style="color:#000000;font-family:Arial;font-size:13px;">.<br><br><br><br></span></div>

            </div>
        </div>
        <img src="images/img0008.png" id="MergedObject1" alt="" title="" style="border-width:0;position:absolute;left:2px;top:184px;width:900px;height:85px;z-index:49">
        <script>
            var vare0 = 0;
            var vare1 = 0;
            var vare2 = 0;
            var vare3 = 0;
            var vare4 = 0;
            var vare5 = 0;
            var vare6 = 0;
            var vare7 = 0;
            var total = 0;

            if (localStorage.getItem("test1") !== null) {
                vare0 = localStorage.getItem("test0");
                vare1 = localStorage.getItem("test1");
                vare2 = localStorage.getItem("test2");
                vare3 = localStorage.getItem("test3");
                vare4 = localStorage.getItem("test4");
                vare5 = localStorage.getItem("test5");
                vare6 = localStorage.getItem("test6");
                vare7 = localStorage.getItem("test7");


            }
            document.getElementById("Editbox0").value = vare0;
            document.getElementById("Editbox1").value = vare1;
            document.getElementById("Editbox2").value = vare2;
            document.getElementById("Editbox3").value = vare3;
            document.getElementById("Editbox4").value = vare4;
            document.getElementById("Editbox5").value = vare5;
            document.getElementById("Editbox6").value = vare6;
            document.getElementById("Editbox7").value = vare7;



        </script>
    </body>
</html>