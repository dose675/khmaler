<%@page import="java.sql.Date"%>
<%@page import="java.time.LocalDate"%>
<%@page import="Entities.User"%>
<%@page import="Entities.Task"%>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Velkommen</title>
        <meta name="generator" content="WYSIWYG Web Builder 12 - http://www.wysiwygwebbuilder.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="1�rsprojekt.css" rel="stylesheet">
        <link href="loggedKunde.css" rel="stylesheet">
        <script>
            function popUp(url) {
                UserWindow = window.open(url, 'ProductAssignmentPage', 'width = 800, height = 900');
                return false;
            }
        </script>
    </head>
    <body>
        <div id="Layer2" style="position:relative;text-align:center;width:100%;height:437px;float:left;clear:left;display:block;z-index:17;">
            <div id="Layer2_Container" style="width:970px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
                <div id="wb_Text1" style="position:absolute;left:243px;top:186px;width:261px;height:2px;z-index:5;">
                    &nbsp;</div>
                <div id="wb_Text2" style="position:absolute;left:271px;top:48px;width:250px;height:2px;z-index:6;">
                    &nbsp;</div>

                <img src="images/img0020.png" id="MergedObject1" alt="" title="" style="border-width:0;position:absolute;left:35px;top:7px;width:900px;height:85px;z-index:8">
                <div id="wb_Text3" style="position:absolute;left:283px;top:178px;width:249px;height:80px;z-index:9;">
                    <span style="color:#000000;font-family:Arial;font-size:13px;"><br>Her kan du se din oprettede opgave. Du kan rette i den efter behov, samt annulere hvis det ikke passer dig<br><br>God forn�jelse. </span></div>
                <div id="wb_Text5" style="position:absolute;left:283px;top:122px;width:359px;height:37px;z-index:10;">
                    <%

                        if (session.getAttribute("username") == null) {
                            session.invalidate();
                            request.setAttribute("errormessage", "Invalid. Du skal logge ind ordentligt!");
                            request.getRequestDispatcher("/index.jsp").forward(request, response);
                        }
                    %>
                    <%
                        Task task = (Task) request.getAttribute("task");
                        User user = (User) session.getAttribute("user");

                        String firstname = user.getFirstname();
                        String lastname = user.getLastname();
                        String address = user.getAddress();
                        int zipcode = user.getZip();
                        int id = user.getId();

                        //Fetching Task data
                        int taskId = task.getTaskId();
                        Date dateCr = task.getDateOfCreation();
                        Date dateJob = task.getDateOfJob();
                        double total = task.getTotalPrice();
                        int taskUserId = task.getCustommerId();

                        System.out.println("taskid: " + taskId + "\nTaskUserId: " + taskUserId + "\nUserid: " + id);

                    %>
                    <h1>Velkommen <%=firstname%></h1></div>
            </div>
        </div>
        <div id="wb_LayoutGrid1">
            <div id="LayoutGrid1">
                <div class="row">
                    <div class="col-1">
                    </div>
                    <div id="" class="UserInfo" style="float:left;text-align: left;">
                        <p>Fornavn: <%=firstname%></p>
                        <p>Efternanvn: <%=lastname%></p>
                        <p>Adresse: <%=address%></p>
                        <p>Postnummer: <%=zipcode%></p>
                        <p>Bruger Id: <%=id%></p>

                        <%
                            String p = (String) request.getAttribute("pword");
                            String u = (String) session.getAttribute("username");
                            if (p != null) {
                        %>
                        <h3 style="background-color: red">VIGTIGT!</h3>
                        <p><b>Dit login er:</b></p>
                        <p>Brugernavn: <%=u%></p>
                        <p>password: <%=p%></p>
                        <p>Du kan �ndre dit password hvis det ikke passer dig</p>
                        <%
                            }
                        %>
                    </div>
                    <div id="" class="ChangePass" style="float:right;text-align: left;padding: 5px;">
                        <h2>Skift Password</h2>
                        <form action="UserManager" method="post">
                            <input type="hidden" name="type" value="update" >
                            <input type="hidden" name="id" value="<%=id%>">
                            <label>Password: </label>
                            <input type="password" name="uPass" disabled>
                            <br>
                            <label>Gentag password:</label>
                            <input type="password" name="password2" disabled>
                            <br>
                        </form>
                    </div>
                    <div class="Task">
                        <%
                            boolean taskValid = id == taskUserId;
                            if (taskValid) {
                        %>
                        <p>Opgave id: <%=taskId%></p>
                        <p>Dato oprettet: <%=dateCr.toLocalDate()%></p>
                        <p>Dato for Udf�rsel(Tids estimat, du bliver kontaktet): <%=dateJob.toLocalDate()%></p>
                        <p><%=total%></p>
                        <%} else { %>   
                        <p>Der blev ikke fundet nogen opgave tilh�rende din bruger!</p>
                        <%}%>
                        <div class="Product">
                            <%
                                if (taskValid) {
                            %>
                            <button class='button' onclick="popUp('/WebApplication2/GetFullProductServlet?id=<%=taskId%>')">Se opgaven!</button>
                            <p>F�r du annullere anbefaler vi at du ringer eller skriver til maleren, hvis du fortsat er interesseret i at f� noget malet</p>
                            <form method='get' action='/WebApplication2/CustomerRequest?id=<%=taskId%>'>
                                <input type='hidden' name='id' value='<%=taskId%>'>
                                <button type='submit' name='logout' value='Annuler Ordre!' class='button'>
                                    Annuler Ordre!
                                </button>
                            </form>
                            <%
                        } else {%>
                            <p>Intet produkt uden opgave</p>
                            <%
                                }
                            %>
                        </div>
                    </div>
                </div>
                <form method='get' action='Logout'>
                    <button style='float:right' type='submit' name='logout' value='Logout' class='button'>
                        Logout
                    </button>
                </form>
            </div>
        </div>
    </body>
</html>