
import CRUD.ProductDao;
import CRUD.ProductDaoImpl;
import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import CRUD.UserDao;
import CRUD.UserDaoImpl;
import Entities.Product;
import Entities.User;
import Security.Security;
import database.connection;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Sole purpose is to test
 *
 * @author Arvid
 */
public class test {

    public static void main(String[] args) throws SQLException {
        Security sec = new Security();
        Connection conn = connection.openConnection();
        
        boolean ja = 36 != 36 && !("a".matches("a"));
        System.out.println(ja);
    }
}

/*
Logins:
Maler:
username: Karl-5
password: sIziubADczTBYzWt

Kunde:
BrugerNavn: Arvid-36
password: yzvUNDOneG

Brugernavn: Mart-37
password: wVEqosjuL

Brugernavn: Carsten-44
password: rHBXIqPbja
 */
