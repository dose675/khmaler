/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CRUD;

import Entities.User;
import java.util.List;

/**
 * Interface for User handling between database and System(Server) 
 * @author Arvid
 */
public interface UserDao {
    
    int createUser(User user);
    
    User getUserById(int id);
    
    //Used for validating the user
    User validate(String firstname, int id , String password);
    
    List<User> findAll();
    
    void updateUser(User user);
    
    void deleteUser(int id);
    
    User getUserByEmail(String email);
    
    void createCustomer(String email, int phoneNr, String fornavn, String efternavn, String adresse, int postNummer, String passwordW);
    
    //void newPassword(String firstname, String oldPassword, String newPassword);
}
