package CRUD;

import Entities.Product;
import java.util.List;

/**
 * Interface for handling products
 * @author Arvid
 */
public interface ProductDao {
    //############################################################
    //Handling products related to inventory
    //############################################################
    void createProduct(String name, int ceil, int wall, int door, int window);
    
    Product getProduct(int paintId);
    
    void deleteProduct(String paintId);
    
    void updateProduct(String id, String name, int ceil, int wall, int door, int window);
    
    List getAllProducts();
    //############################################################
    //OVER
    //############################################################
    
    //############################################################
    //Handles assignment/task related products
    //############################################################
    List getRelatedProducts(int assignmentId);
    
    void removeProductFromTask(String productName, int assignmentId);
    
    void updateTaskProduct(Product product);
    
    void addProductToTask(Product product);
    
    void assignmentProducts(int assignmentId, String productName, String paint, int amount);
    //############################################################
    //OVER
    //############################################################
}
