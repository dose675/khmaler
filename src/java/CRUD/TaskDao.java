package CRUD;

import Entities.Task;
import Entities.User;
import java.util.List;

/**
 * Interface for handling task connection to DB
 *
 * @author Arvid
 */
public interface TaskDao {
        
    int createTask(Task task);
    
    Task getTaskById(int id);
    
    List<Task> findAll();
    
    void updateTask(Task task);
    
    void deleteTask(int id);
    
    List<User> getRelatedPainters(int assignmentId);
    
    Task getTaskByCustommerId(int id);
    
    void createManualTask(Task task);
    
    void assignPainter(int taskId, int painterId);
    
    void removePainter(int taskId, int painterId);
}
