package CRUD;

import Entities.Task;
import Entities.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles all connections to the database related to the Task object
 * This is responsible for inserting, deleting, updating and retreive data of tasks
 * It extends the TaskDao interface
 * @author Arvid
 */
public class TaskDaoImpl extends sharedVariables implements TaskDao {

    private static final String GET_TASK_CUSTOMMERID = "SELECT * FROM assignment WHERE custommer=?";
    private final static String GET_TASKS = "SELECT * FROM assignment ORDER BY assignment_id DESC";
    private final static String INSERT_TASK = "INSERT INTO assignment(dateCr, dateJob, total, custommer) VALUES(?,?,?,?)";
    private final static String GET_TASK_BY_ID = "SELECT * FROM assignment WHERE assignment_id=?";
    private final static String GET_PAINTERS = "SELECT painter_assignment.*, users.* FROM KeaOpgave.painter_assignment "
            + "INNER JOIN KeaOpgave.users ON painter_assignment.painter=users.user_id\n"
            + "WHERE assignment=?";
    private final static String DELETE_TASK = "DELETE FROM assignment WHERE assignment_id=?";
    private final static String UPDATE_TASK = "UPDATE assignment SET dateCr=? , dateJob=? , total=? , custommer=? WHERE assignment_id=?";
    private final static String ASSIGN_PAINTER = "INSERT INTO painter_assignment VALUES(?,?)";
    private final static String REMOVE_PAINTER = "DELETE FROM painter_assignment WHERE assignment=? AND painter=?";

    /**
     * Opens the connection to the database
     *
     * @param conn SQL connection
     */
    public TaskDaoImpl(Connection conn) {
        this.conn = conn;
    }

    /**
     * Creates a new task and sends it to the database. This method will
     * automatically create creation date(today) and estimated date of job(in 4
     * weeks), the task object must contain total price and customer id
     *
     * @param task Object to be created
     * @return Auto Incremented ID
     */
    @Override
    public int createTask(Task task) {
        LocalDate dateCr = LocalDate.now();
        int result = 0;
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(INSERT_TASK, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setDate(1, Date.valueOf(dateCr));
            stmt.setDate(2, Date.valueOf(dateCr.plusWeeks(4)));
            stmt.setDouble(3, task.getTotalPrice());
            stmt.setInt(4, task.getCustommerId());
            result = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                result = rs.getInt(1);
            }
            return result;
        } catch (SQLException e) {
        }
        return result;
    }

    /**
     * Returns a Task from its ID from the database Connects to the database and
     * retreives a specific task from the unique id given, reads all the data
     * into a task object and returns it
     *
     * @param taskId exact id of task
     * @return Task Found task as an object
     */
    @Override
    public Task getTaskById(int taskId) {
        Task task = new Task();
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(GET_TASK_BY_ID);
            stmt.setInt(1, taskId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                task.setTaskId(rs.getInt("assignment_id"));
                task.setDateOfCreation(rs.getDate("dateCr"));
                task.setDateOfJob(rs.getDate("dateJob"));
                task.setTotalPrice(rs.getDouble("total"));
                task.setCustommerId(rs.getInt("custommer"));
            }
        } catch (SQLException e) {
        }
        return task;
    }

    /**
     * Returns all task currently registered from the database Connects the the
     * database and finds all tasks inserted, in the result set we read all the
     * rows into task objects, adds the objects to the list and returns the list
     *
     * @return list of Task Objects
     */
    @Override
    public List<Task> findAll() {
        List<Task> taskList = new ArrayList<>();
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(GET_TASKS);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Task task = new Task();
                task.setTaskId(rs.getInt("assignment_id"));
                task.setDateOfCreation(rs.getDate("dateCr"));
                task.setDateOfJob(rs.getDate("dateJob"));
                task.setTotalPrice(rs.getDouble("total"));
                task.setCustommerId(rs.getInt("custommer"));
                taskList.add(task);
            }
        } catch (SQLException e) {

        }
        return taskList;
    }

    /**
     * For updating a task, takes a task as a parameter, finds it by id and
     * updates it accordingly
     *
     * @param task Object to be updated
     */
    @Override
    public void updateTask(Task task) {
        //Der er et library fra apache som kan sammenligne beans, men det kan vi 
        //vente med til vi får maven på alle computere, hvis vi gør det
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(UPDATE_TASK);
            stmt.setDate(1, task.getDateOfCreation());
            stmt.setDate(2, task.getDateOfJob());
            stmt.setDouble(3, task.getTotalPrice());
            stmt.setInt(4, task.getCustommerId());
            stmt.setInt(5, task.getTaskId());
            stmt.executeUpdate();
        } catch (Exception e) {
        }
    }

    /**
     * Deletes a task by id The id given must match a tasks id, this method
     * executes an update where the given task shall be removed from the
     * database, which will also remove "products_assignment"
     *
     * @param id exact id to be removed from the database
     */
    @Override
    public void deleteTask(int id) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(DELETE_TASK);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Gets all connected painters for an assignment param assignmentId This can
     * check a task to see which painters are related to it, and if they're
     * related they'll be put into a list and returned to be shown
     *
     * @param assignmentId assignment id of task
     * @return List of Painters, user objects
     */
    @Override
    public List<User> getRelatedPainters(int assignmentId) {
        List<User> list = new ArrayList<>();
        try {
            statement = conn.prepareStatement(GET_PAINTERS);
            statement.setInt(1, assignmentId);
            rs = statement.executeQuery();
            while (rs.next()) {
                User painter = new User();
                painter.setFirstname(rs.getString("firstname"));
                painter.setLastname(rs.getString("lastname"));
                painter.setPhone(rs.getInt("phone"));
                painter.setEmail(rs.getString("email"));
                list.add(painter);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    /**
     * To get the latest related task to the custommer id given This is capable
     * of retreiving a task which is related to a customer with all the data for
     * a task, it returns the task for it to be shown
     *
     * @param id exact id of user
     * @return Task Object
     */
    @Override
    public Task getTaskByCustommerId(int id) {
        Task task = new Task();
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(GET_TASK_CUSTOMMERID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                task.setTaskId(rs.getInt("assignment_id"));
                task.setDateOfCreation(rs.getDate("dateCr"));
                task.setDateOfJob(rs.getDate("dateJob"));
                task.setTotalPrice(rs.getDouble("total"));
                task.setCustommerId(rs.getInt("custommer"));
            }
        } catch (SQLException e) {
        }
        return task;
    }

    /**
     * For creating a manual task, if you do wish to specify dates This gives
     * the opportunity to create a task where the painter can specify the dates,
     * normally they're auto generated. And total price and customer id must be
     * given, customer id must match a customer
     *
     * @param task object to be created
     */
    @Override
    public void createManualTask(Task task) {
        //task = checkDates(task);
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(INSERT_TASK);
            stmt.setDate(1, task.getDateOfCreation());
            stmt.setDate(2, task.getDateOfJob());
            stmt.setDouble(3, task.getTotalPrice());
            stmt.setInt(4, task.getCustommerId());
            stmt.executeUpdate();
        } catch (SQLException e) {
        }
    }

//<editor-fold defaultstate="collapsed" desc="NOT IMPLEMENTED, What is this?">
    /*    //Hvad skal vi bruge denne her til?
    private Task checkDates(Task task) {
    if (task.getDateOfCreation() == null) {
    task.setDateOfCreation(Date.valueOf(LocalDate.now()));
    }
    if (task.getDateOfJob() == null) {
    task.setDateOfJob(Date.valueOf(LocalDate.now().plusWeeks(4)));
    }
    return task;
    }*/
//</editor-fold>
    /**
     * Adds a painter to an assignment Task and painter id must match a task to
     * attach the given painter
     *
     * @param taskId task id to which a painter needs to be attached
     * @param painterId specific painter to attach
     */
    @Override
    public void assignPainter(int taskId, int painterId) {
        try {
            statement = conn.prepareStatement(ASSIGN_PAINTER);
            statement.setInt(1, taskId);
            statement.setInt(2, painterId);
            statement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    /**
     * Removes a painter from an assignment task id and painter id must match a
     * given task, and a given painter assigned to that task
     *
     * @param taskId task id from which a painter should be removed
     * @param painterId specific painter id of painter to be removed
     */
    @Override
    public void removePainter(int taskId, int painterId
    ) {
        try {
            statement = conn.prepareStatement(REMOVE_PAINTER);
            statement.setInt(1, taskId);
            statement.setInt(2, painterId);
            statement.executeUpdate();
        } catch (SQLException e) {

        }
    }

}
