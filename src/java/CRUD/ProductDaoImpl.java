package CRUD;

import Entities.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles logic related to product management The table is products_assignment,
 * which is a binding table inbetween assignment and products Products is a
 * static with the selection the painter currently offers and is also maintained
 * here To uniquely identify a Tuple we use assignemnt id and product id, which
 * should only be present on place
 *
 * @author Arvid
 */
public class ProductDaoImpl extends sharedVariables implements ProductDao {
//<editor-fold defaultstate="collapsed" desc="OUTPHASED, NOT IMPLEMENTED">
    /*private static final String PTABLE = "products";
    private static final String GET_ALL = "SELECT * FROM " + PTABLE;
    private static final String GET_PRODUCT = "SELECT * FROM " + PTABLE + " WHERE product_id=?";
    private static final String DELETE_PRODUCT = "DELETE FROM " + PTABLE + " WHERE product_id=?";
    private static final String UPDATE_PRODUCT = "UPDATE " + PTABLE + " SET product_id=?, name=?, price=?, paint=?";
    private static final String INSERT_PRODUCT = "INSERT INTO " + PTABLE + " VALUES(null,?,?,?)";//Name, price and paint
    */
//</editor-fold>
    
    private static final String POTABLE = "paintOptions";
    private static final String GET_ALL_PAINT = "SELECT * FROM " + POTABLE;
    private static final String GET_PAINT = "SELECT * FROM " + POTABLE + " WHERE idpaintOptions=?";//NOT IMPLEMENTED
    private static final String DELETE_PAINT = "DELETE FROM " + POTABLE + " WHERE paintName=?";
    private static final String UPDATE_PAINT = "UPDATE paintOptions SET paintName=?, ceil=?, wall=?, door=?, window=? WHERE paintName=?;";
    private static final String INSERT_PAINT = "INSERT INTO " + POTABLE + " VALUES(?, ?,?,?,?)";//Name, ceil, wall, door, window
    
    private static final String PATABLE = "products_assignment";
    private static final String GET_ALL_RELATED = "SELECT products_assignment.*, products.* FROM KeaOpgave.products_assignment "
            + "INNER JOIN KeaOpgave.products ON products_assignment.product=products.name\n"
            + "WHERE assignment=?";
    private static final String DELETE_BY_FKS = "DELETE FROM " + PATABLE + " WHERE product=? AND assignment=?";
    //private static final String INSERT_PRODUCT_ASSIGNMENT = "INSERT INTO " + PATABLE + " VALUES(?,?,?,?)";//assignment(id), product(name) and amount
    private static final String UPDATE_PA = "UPDATE " + PATABLE + " SET product=?, paint=?, amount=? WHERE assignment=? AND product=?";
    private final static String INSERT_PRODUCTS = "INSERT INTO products_assignment (assignment, product, paint, amount) VALUES (?,?,?,?)";

    public ProductDaoImpl(Connection conn) {
        this.conn = conn;
    }

    //##########################################################
    //Handles products in stock
    //##########################################################
    /**
     * Creating new paint option in stock
     * name is the exact name of the paint to be created, ceil, wall, door, window
     * is properties of on/off = 1/0
     * @param name product name as string
     * @param ceil property on/off 1/0
     * @param wall property on/off 1/0
     * @param door property on/off 1/0
     * @param window property on/off 1/0
     */
    @Override
    public void createProduct(String name, int ceil, int wall, int door, int window) {
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(INSERT_PAINT);
            stmt.setString(1, name);
            stmt.setInt(2, ceil);
            stmt.setInt(3, wall);
            stmt.setInt(4, door);
            stmt.setInt(5, window);
            stmt.executeUpdate();
        } catch (SQLException e) {
        }
    }

//<editor-fold defaultstate="collapsed" desc="UNDER MAINTANANCE, NOT IMPLEMENTED">
    /**
     * Returns a paints from stock
     *
     * @param paintId exact paint to be returned
     * @return Product as an object
     */
    @Override
    public Product getProduct(int paintId) {
        Product product = new Product();
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(GET_PAINT);
            stmt.setInt(1, paintId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                //product.setProductId(rs.getInt("product_id"));
                product.setProductName(rs.getString("name"));
                product.setPrice(rs.getInt("price"));
                product.setPaint(rs.getString("paint"));
            }
        } catch (SQLException e) {
            
        }
        return product;
    }
//</editor-fold>

    /**
     * For removing paints from stock
     * paint id, which is the name, must be the exact same as the on to be removed
     * @param paintId as a string, must be exact name
     */
    @Override
    public void deleteProduct(String paintId) {
        try {
            statement = null;
            statement = conn.prepareStatement(DELETE_PAINT);
            statement.setString(1, paintId);
            statement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    /**
     * For Updating a paint in stock
     * Id is the exact name of the paint to be updated, ceil, wall, door and window
     * is on/off parameters shown by 1/0
     * @param id exact paint name
     * @param name name of paint
     * @param ceil property on/off 1/0
     * @param wall property on/off 1/0
     * @param door property on/off 1/0
     * @param window property on/off 1/0
     */
    @Override
    public void updateProduct(String id, String name, int ceil, int wall, int door, int window) {
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(UPDATE_PAINT);
            stmt.setString(1, name);
            stmt.setInt(2, ceil);
            stmt.setInt(3, wall);
            stmt.setInt(4, door);
            stmt.setInt(5, window);
            stmt.setString(6, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
        }
    }
    
    //<editor-fold defaultstate="collapsed" desc="UNDER MAINTANANCE">
    /**
     * Returns all paints from the inventory (database)
     *
     * @return List of all products
     */
    @Override
    public List<Product> getAllProducts() {
        List<Product> list = new ArrayList<>();
        
        try {
            statement = conn.prepareStatement(GET_ALL_PAINT);
            rs = statement.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setPrice(rs.getInt("price"));
                //p.setProductId(rs.getInt("product_id"));
                p.setProductName(rs.getString("name"));
                p.setPaint(rs.getString("paint"));
                list.add(p);
            }
        } catch (SQLException e) {
        }
        return list;
    }
//</editor-fold>
    
    //##########################################################
    //Over
    //##########################################################

    //##########################################################
    //Handles products related to task
    //##########################################################
    /**
     * Returns all related products a task might have
     * connects to the database and retrieves all assignments/tasks
     * with all the data needed for each task reads them into an a 
     * list and returns that list
     * @param assignmentId id of task to get products of
     * @return List of Products
     */
    @Override
    public List getRelatedProducts(int assignmentId) {
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(GET_ALL_RELATED);
            stmt.setInt(1, assignmentId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setAssignmentId(rs.getInt("assignment"));
                product.setProductName(rs.getString("product"));
                product.setAmount(rs.getInt("amount"));
                product.setPrice(rs.getInt("price"));
                product.setPaint(rs.getString("paint"));
                list.add(product);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    /**
     * Removes a product from a given task, it needs an id for the task, and the
     * name of the product to be removed
     *
     * @param productName string as name identifier
     * @param assignmentId exact integer for assignment
     */
    @Override
    public void removeProductFromTask(String productName, int assignmentId) {
        try {
            statement = conn.prepareStatement(DELETE_BY_FKS);
            statement.setString(1, productName);
            statement.setInt(2, assignmentId);
            statement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    /**
     * For updating a product which is assigned to a task, will update it
     * according to the Object of Product it recieves(assignment id, product and
     * amount)
     *
     * @param product object
     */
    @Override
    public void updateTaskProduct(Product product) {
        try {
            statement = conn.prepareStatement(UPDATE_PA);
            statement.setString(1, product.getProductName());
            statement.setString(2, product.getPaint());
            statement.setInt(3, product.getAmount());
            statement.setInt(4, product.getAssignmentId());
            statement.setString(5, product.getProductName());
            statement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    /**
     * For adding a product to a task manually
     * Takes a product object as a parameter, which mus contain
     * assignment id, product name, paint name and amount
     * @param product object
     */
    @Override
    public void addProductToTask(Product product) {
        try {
            statement = conn.prepareStatement(INSERT_PRODUCTS);
            statement.setInt(1, product.getAssignmentId());
            statement.setString(2, product.getProductName());
            statement.setString(3, product.getPaint());
            statement.setInt(4, product.getAmount());
            statement.executeUpdate();
        } catch (SQLException e) {

        }
    }

    /**
     * Handles creating products for a given task
     * Parameters must have exact assignment id for it to be properly 
     * inserted into the database, productName must also match a product
     * from the database "products" as well as paint, the amount is work 
     * area to be painted
     * @param assignmentId exact id to be handled
     * @param productName exact product name as unique identifier
     * @param paint paint name to be used for task
     * @param amount overall area to be painted
     */
    @Override
    public void assignmentProducts(int assignmentId, String productName, String paint, int amount) {
        try {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(INSERT_PRODUCTS);
            stmt.setInt(1, assignmentId);
            stmt.setString(2, productName);
            stmt.setString(3, paint);
            stmt.setInt(4, amount);
            stmt.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //##########################################################
    //OVER
    //##########################################################
}
