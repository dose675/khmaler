package CRUD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Handles retrieving data related to paint options, to show in the "Opgave.jsp"
 *
 * @author Andreas
 */
public class display extends sharedVariables {

    /*private final static String GET_TASKS = "SELECT * FROM assignment ORDER BY assignment_id DESC";*/
    private final static String GET_PRODUCTPRICE = "SELECT price FROM products";

    public display(Connection conn) {
        this.conn = conn;
    }

    /**
     * This method reads from the users table, and returns a list of all user
     * ids in descending order
     *
     * @return a list of user id
     */
    public ArrayList<Integer> getUserId() {
        ArrayList<Integer> userIdList = new ArrayList<>();

        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("SELECT * FROM users ORDER BY user_id DESC");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                userIdList.add(rs.getInt("user_id"));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return userIdList;
    }

    /**
     * Retreives product names Connects to the database and retrives an id from
     * a product name This is outphased and must be rewritten or database
     * updated
     *
     * @param product
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public int displayProducts(String product) throws SQLException, Exception {
        int id = 0;
        String getProducts = "SELECT * FROM products WHERE name = ?";
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(getProducts);
            stmt.setString(1, product);
            ResultSet rs = stmt.executeQuery();
            id = rs.getInt("product_id");

        } catch (SQLException e) {
        }
        return id;
    }

    /**
     * Retreives product pricing Connects to the database, reads all product
     * prices into an array and returns that array of product prices(integers)
     *
     * @return
     * @throws Exception
     */
    public ArrayList<Integer> getProductPrice() throws Exception {
        ArrayList<Integer> priceList = new ArrayList<>();

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(GET_PRODUCTPRICE);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                priceList.add(Integer.parseInt(rs.getString("price")));
            }
        } catch (NumberFormatException | SQLException e) {
            System.out.println(e);
        }
        return priceList;
    }

    /**
     * Retreives paint for doors Connects to the database and retreives data
     * about paint, for doors, where the door column has a "1"
     *
     * @return List of paint names
     * @throws SQLException
     * @throws Exception
     */
    public ArrayList<String> displayPaintDoor() throws SQLException, Exception {

        displaylist = new ArrayList<>();
        String getPaints = "SELECT * FROM paintOptions WHERE door = 1";

        rs = null;
        try {
            //STEP 4: Execute a query

            statement = conn.prepareStatement(getPaints);
            rs = statement.executeQuery();
            while (rs.next()) {
                displaylist.add(rs.getString("paintName"));
            }
            rs.close();
            statement.close();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return displaylist;
    }

    /**
     * Retreives paint for ceiling Connects to the database and retreives data
     * about ceiling paint, returns an array of paint names which can be used on
     * ceiling
     *
     * @return List of paint names
     */
    public ArrayList<String> displayPaintCeiling() {
        displaylist = new ArrayList<>();

        String getPaints = "SELECT * FROM paintOptions WHERE ceil = 1";

        rs = null;
        try {
            //STEP 4: Execute a query

            statement = conn.prepareStatement(getPaints);
            rs = statement.executeQuery();
            while (rs.next()) {
                displaylist.add(rs.getString("paintName"));
            }
            rs.close();
            statement.close();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return displaylist;
    }

    /**
     * Retreives paint for walls Connects to the database and retreives data
     * about walls paint, returns an array of paint names which can be used on
     * walls
     *
     * @return List of paint names
     */
    public ArrayList<String> displayPaintWall() {
        displaylist = new ArrayList<>();

        String getPaints = "SELECT * FROM paintOptions WHERE wall = 1";

        rs = null;
        try {
            //STEP 4: Execute a query

            statement = conn.prepareStatement(getPaints);
            rs = statement.executeQuery();
            while (rs.next()) {
                displaylist.add(rs.getString("paintName"));
            }
            rs.close();
            statement.close();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return displaylist;
    }

    /**
     * Retreives paint for windows Connects to the database and retreives data
     * about window paint, returns an array of paint names which can be used on
     * window frames
     *
     * @return List of paint names
     */
    public ArrayList<String> displayPaintWindows() {
        displaylist = new ArrayList<>();

        String getPaints = "SELECT * FROM paintOptions WHERE window = 1";

        rs = null;
        try {
            //STEP 4: Execute a query
            statement = conn.prepareStatement(getPaints);
            rs = statement.executeQuery();
            while (rs.next()) {
                displaylist.add(rs.getString("paintName"));
            }
            rs.close();
            statement.close();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return displaylist;
    }

}
