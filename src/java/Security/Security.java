package Security;
import java.security.*;
import java.util.Random;

/**
 * Handles Password Security, main purpose is to hash passwords.
 * But is also capable of creating new passwords and salts
 * @author Arvid
 */
public class Security {
    
    
    /**
     * Generates a random password
     * Automatically hashes it befor sending it back
     * @return Password as random string between 8 and 12 characters(a-z + A-Z)
     */
    public String newPassword(){
        Random r = new Random();
        String alphabet =  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb =  new StringBuilder();
        for(int i = 0; i < (r.nextInt(5)+8); i++){
            sb.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        return sb.toString();
    }
    /**
     * Generates random salt to use with passwords
     * @return Salt random string between 10 and 29 characters as a String(a-z, a-Z,0-9 + -+=.,()/{[]}_*)
     */
    //<editor-fold defaultstate="collapsed" desc="Salt NOT IMPLEMENTED">
    public String newSalt(){
        Random r = new Random();
        String alphabet =  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+=.,()/{[]}_*";
        StringBuilder sb = new StringBuilder();
        for(int i =  0; i < (r.nextInt(20)+10); i ++)
            sb.append(alphabet.charAt(r.nextInt(alphabet.length())));
        return sb.toString();
    }
//</editor-fold>
   
    /**
     * Hashes the given password to SHA256
     * @param password to be hashed with SHA256
     * @return Hashed Password SHA-256 as hex stringg
     */
    public String hashPassword(String password){
        String codeToHash = password;
        byte [] input = codeToHash.getBytes();
        StringBuffer hexDigest = null;
        try {
            //Get a message digest object using SHA256 algorithm
            MessageDigest SHA256 = MessageDigest.getInstance("ShA-256");
            //compute digest
            SHA256.update(input);
            byte [] digest = SHA256.digest();
            //Converting byte to hex
            hexDigest = new StringBuffer();
            for(int i = 0; i < digest.length; i++){
                hexDigest.append(Integer.toString((digest[i]&0xff)+0x100,16).substring(1));
                //Making it Hex
            }
        } catch (NoSuchAlgorithmException e) {
        }
        return hexDigest.toString();
    }
    
}
