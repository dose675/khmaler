package Entities;

/**
 * Product POJO for handling stock
 *
 * @author Arvid
 */
public class Product {

    private int assignmentId;
    private String productName;
    private int price;//stock
    private String paint;
    //private Paint paint1;
    private int amount;

    /**
     * For instanciating an empty Task Object
     */
    public Product() {
        super();
    }

    /**
     * Instanciate product with all attributes
     *
     * @param productName product name as string
     * @param price price of the product as int
     * @param paint what paint is used for the product
     * @param amount the overall area to be covered
     * @param assignmentId what assignment/task is it attached
     */
    public Product(String productName, int price, String paint, int amount, int assignmentId) {
        this.productName = productName;
        this.price = price;
        this.paint = paint;
        this.amount = amount;
        this.assignmentId = assignmentId;
    }

    /**
     * Instanciate product with limited attributes
     * Redundant, not neccesary
     * @param productName product name as string
     * @param paint what paint is used for the product
     * @param price price of the product as int
     */
    public Product(String productName, int price, String paint) {
        this.productName = productName;
        this.price = price;
        this.paint = paint;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPaint() {
        return paint;
    }

    public void setPaint(String paint) {
        this.paint = paint;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }

}
