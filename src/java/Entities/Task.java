package Entities;

import java.sql.Date;
import java.util.List;

/**
 * Task POJO
 *
 * @author Arvid
 */
public class Task {

    private int taskId;
    private Date dateOfCreation;
    private Date dateOfJob;
    private double totalPrice;
    private int custommerId; //Foreign key to user tabel
    List<Product> productList;
    //List<User> painterList; //Theoretically this should be in too

    /**
     * For instanciating an empty Task Object
     */
    public Task() {
        super();
    }

    /**
     * For creating a task object
     *
     * @param opgaveId id of the task
     * @param dateOfCreation date the task is created SQL Date
     * @param dateOfJob date the task is set to be done SQL Date
     * @param totalPrice total price of the task, as double
     * @param custommerId custommer to which the task is attached
     */
    public Task(int opgaveId, Date dateOfCreation, Date dateOfJob, double totalPrice, int custommerId) {
        this.taskId = opgaveId;
        this.dateOfCreation = dateOfCreation;
        this.dateOfJob = dateOfJob;
        this.totalPrice = totalPrice;
        this.custommerId = custommerId;
    }


    public Task(Date dateOfCreation, Date dateOfJob, double totalPrice, int custommerId) {
        this.dateOfCreation = dateOfCreation;
        this.dateOfJob = dateOfJob;
        this.totalPrice = totalPrice;
        this.custommerId = custommerId;
    }
    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getDateOfJob() {
        return dateOfJob;
    }

    public void setDateOfJob(Date dateOfJob) {
        this.dateOfJob = dateOfJob;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getCustommerId() {
        return custommerId;
    }

    public void setCustommerId(int custommerId) {
        this.custommerId = custommerId;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

}
