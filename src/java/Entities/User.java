package Entities;

/**
 * User Bean, handles all types of users in the system
 * @author Arvid
 */
public class User {
    private int id;
    private String firstname;
    private String lastname;    
    private String password;
    private String email;
    private int phone;
    private String role; // Måske er en boolean lettere at arbejde med
    
    private String address;
    private int zip;
    private String city;

    /**
     * For Instanciating an empty user Object
     */
    public User(){
        super();
    }
    
    /**
     * Bean for the Painter, no address or zip 
     * @param id id of the user
     * @param firstname users firstname
     * @param lastname users lastname
     * @param password users password
     * @param email users email
     * @param phone users phone number
     * @param role user role(painter/customer)
     */
    public User(int id, String firstname, String lastname, String password, String email, int phone, String role) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.role = role;
    }
    /**
     * Bean for the users, with address and zip
     * @param id id of the user
     * @param firstname users firstname
     * @param lastname users lastname
     * @param password users password
     * @param email users email
     * @param phone users phone number
     * @param role user role(painter/customer)
     * @param address users address
     * @param zip users zip code, is crucial
     */
    public User(int id, String firstname, String lastname, String password, String email, int phone, String role, String address, int zip) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.address = address;
        this.zip = zip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
    
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    
    
}
