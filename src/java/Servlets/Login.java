package Servlets;

import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import CRUD.UserDao;
import CRUD.UserDaoImpl;
import Entities.Task;
import Entities.User;
import Security.Security;
import database.connection;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet for authorizing logins
 *
 * @author Arvid
 */
@WebServlet(name = "Login")
public class Login extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = getUser(request);
        String page;
        if (user != null) {
            TaskDao td = new TaskDaoImpl(connection.openConnection());
            if (user.getRole().toLowerCase().equals("maler")) {
                page = "/loggedMaler.jsp";
                request.setAttribute("tasklist", td.findAll());
            } else {
                Task task = td.getTaskByCustommerId(user.getId());
                request.setAttribute("task", task);
                page = "/loggedKunde.jsp";
            }
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            session.setAttribute("username", request.getParameter("username"));
        } else {
            request.setAttribute("errorMessage", "Invalid Credentials");
            page = "/Login.jsp";
        }
        request.getRequestDispatcher(page).forward(request, response);
    }

    /**
     * Handles retrieving the parameters and validating the user
     *
     * @param request servlet request
     * @return User object
     */
    private User getUser(HttpServletRequest request) {
        User user = null;
        Security sec = new Security();
        String username = request.getParameter("username");
        String password = sec.hashPassword(request.getParameter("password"));
        String[] name = username.split("-");
        String firstname = name[0];
        int id = 0;
        try {//Skal laves til if statement eller 
            id = Integer.parseInt(name[1]);
        } catch (NumberFormatException e) {
        }
        UserDao ud = new UserDaoImpl(connection.openConnection());
        user = ud.validate(firstname, id, password);
        return user;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
