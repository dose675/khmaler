package Servlets;

import CRUD.display;

import database.connection;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Calculates a price estimate for the user
 * @author Andreas
 */
@WebServlet(name = "calclulateServlet", urlPatterns = {"/calclulateServlet"})
public class calclulateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        int ceil = Integer.parseInt(request.getParameter("Editbox0"));
        int wall = Integer.parseInt(request.getParameter("Editbox1"));

        session.setAttribute("wall", wall);
        session.setAttribute("ceil", ceil);
        String paintForWall = request.getParameter("Editbox2");
        session.setAttribute("paintWall", paintForWall);
        String paintForCeil = request.getParameter("Editbox3");
        session.setAttribute("paintCeil", paintForCeil);

        int doors = Integer.parseInt(request.getParameter("Editbox4"));
        session.setAttribute("doors", doors);
        String paintForDoors = request.getParameter("Editbox5");
        session.setAttribute("paintDoors", paintForDoors);
        int windows = Integer.parseInt(request.getParameter("Editbox6"));
        session.setAttribute("windows", windows);
        String paintForWindows = request.getParameter("Editbox7");
        session.setAttribute("paintWindows", paintForWindows);
        double total = calculate(wall, ceil, doors, windows);
        String calc = request.getParameter("calc");
        String creat = request.getParameter("create");

        if (calc != null) {
            request.setAttribute("totalPrice", total);
            System.out.println(total);
            System.out.println("Testing servlet" + request.getAttribute("totalPrice"));
            request.getRequestDispatcher("Opgaver.jsp").forward(request, response);
        }
        if (creat != null) {

            display disp = new display(connection.openConnection());
            int lastUserId = disp.getUserId().get(0);
            int id = lastUserId + 1;
            session.setAttribute("newUserId", id);
            session.setAttribute("totalPrice", total);
            request.getRequestDispatcher("Support_&_login.jsp").forward(request, response);

        }
    }

    public double calculate(int walls, int ceil, int doors, int windows) {
        display prices = new display(connection.openConnection());
        int total = 0;
        ArrayList<Integer> priceList = new ArrayList<>();

        try {
            priceList = prices.getProductPrice();
            int wallAndCeilPrice = priceList.get(0);
            int doorsPrice = priceList.get(4);
            int windowsPrice = priceList.get(2);

            total = wallAndCeilPrice * walls + doorsPrice * doors + windowsPrice * windows + ceil * wallAndCeilPrice;
        } catch (Exception ex) {

        }
        return total;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
