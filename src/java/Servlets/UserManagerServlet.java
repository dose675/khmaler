package Servlets;

import CRUD.TaskDaoImpl;
import CRUD.UserDaoImpl;
import Entities.User;
import database.connection;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles user management
 * Features: create, delete and update user
 * @author Arvid
 */
@WebServlet(name = "UserManagerServlet")
public class UserManagerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("username");
        if (userid != null) {
            String input = request.getParameter("type");
            UserDaoImpl ud = new UserDaoImpl(connection.openConnection());

            switch (input) {
                case "create":
                    create(request, ud);
                    break;
                case "update":
                    update(request, ud);
                    break;
                case "delete":
                    delete(request, ud);
                    break;
            }
        }
        TaskDaoImpl td = new TaskDaoImpl(connection.openConnection());
        request.setAttribute("tasklist", td.findAll());
        request.getRequestDispatcher("/loggedMaler.jsp").forward(request, response);
    }

    /**
     * For retrieving form data, and packing it into a user, then send it
     * further
     *
     * @param request servlet request
     * @param ud UserDaoImpl to write the new user to the DB
     */
    private void create(HttpServletRequest request, UserDaoImpl ud) {
        String firstname = request.getParameter("uFirst");
        System.out.println(firstname);
        String lastname = request.getParameter("uLast");
        System.out.println(lastname);
        String password = request.getParameter("uPass");
        String email = request.getParameter("uEmail");
        int phone = Integer.parseInt(request.getParameter("uPhone"));
        System.out.println(phone);
        String role = request.getParameter("uRole");
        String address = request.getParameter("uAddress");
        int zip = Integer.parseInt(request.getParameter("uZip"));
        String city = request.getParameter("uCity");
        User user = new User(0, firstname, lastname, password, email, phone, role, address, zip);
        ud.createUser(user);
        request.setAttribute("cMessage", ("Opgave, med id: " + " er blevet opdateret"));
    }

    /**
     * For retrieving form data, comparing to the old user, then sending it
     * further
     * 
     * @param request servlet request
     * @param ud UserDaoImpl to update user
     */
    private void update(HttpServletRequest request, UserDaoImpl ud) {
        int id = Integer.parseInt(request.getParameter("id"));
        User oldUser = ud.getUserById(id);
        User user = new User();
        user.setId(id);
        System.out.println(oldUser.getRole());
        if (oldUser.getRole().equals("maler")) {
            user.setFirstname(!"".equals(request.getParameter("uFirst")) ? request.getParameter("uFirst") : oldUser.getFirstname());
            user.setLastname(!"".equals(request.getParameter("uLast")) ? request.getParameter("uLast") : oldUser.getLastname());
            user.setAddress(!"".equals(request.getParameter("uAddress")) ? request.getParameter("uAddress") : oldUser.getAddress());
            user.setPassword(!"".equals(request.getParameter("uPass")) ? request.getParameter("uPass") : oldUser.getPassword());
            user.setEmail(!"".equals(request.getParameter("uEmail")) ? request.getParameter("uEmail") : oldUser.getEmail());
            user.setPhone(!"".equals(request.getParameter("uPhone")) ? Integer.parseInt(request.getParameter("uPhone")) : oldUser.getPhone());
            user.setRole(!"".equals(request.getParameter("uRole")) ? request.getParameter("uRole") : oldUser.getRole());

            //String city = !"".equals(request.getParameter("uCity")) ? request.getParameter("uCity") : oldUser.getFirstname();
        } else {
            user = oldUser;
            user.setPassword(request.getParameter("uPass"));
        }
        ud.updateUser(user);
        request.setAttribute("eMessage", ("Bruger, med id: " + id + " er blevet opdateret"));

    }

    /**
    * To delete User
    * @param request servlet request
    * @param ud UserDaoImpl to delete user from DB
    */
    private void delete(HttpServletRequest request, UserDaoImpl ud) {
        int id = Integer.parseInt(request.getParameter("delete"));
        ud.deleteUser(id);
        request.setAttribute("dMessage", "Opgave med id: " + id + " er blevet slettet!");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
