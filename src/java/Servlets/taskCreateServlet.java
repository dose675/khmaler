package Servlets;

import CRUD.ProductDao;
import CRUD.ProductDaoImpl;
import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import CRUD.UserDao;
import CRUD.UserDaoImpl;
import Entities.Task;
import Entities.User;
import Security.Security;
import database.connection;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles signup of user, and is responsible for creating the task and products too
 * @author Andreas
 */
@WebServlet(name = "taskCreateServlet", urlPatterns = {"/taskCreateServlet"})
public class taskCreateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        TaskDao td = new TaskDaoImpl(connection.openConnection());
        UserDao ud = new UserDaoImpl(connection.openConnection());
        ProductDao pd = new ProductDaoImpl(connection.openConnection());

        //person information
        Security pass = new Security();
        String password = pass.newPassword();
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String address = request.getParameter("address");
        int zipcode = Integer.parseInt(request.getParameter("zip"));//Bliver nok nødt til at have en try and catch
        int phone = Integer.parseInt(request.getParameter("phone"));
        String email = request.getParameter("email");
        User user = new User(0, firstname, lastname, password, email, phone, "kunde", address, zipcode);
        int newUserId = ud.createUser(user);
        user.setId(newUserId);
        //price of assignment
        double total = (Double) session.getAttribute("totalPrice");

        //Assignment information
        int wall = (Integer) session.getAttribute("wall");
        int ceil = (Integer) session.getAttribute("ceil");
        String paintForWall = session.getAttribute("paintWall").toString();
        String paintForCeil = session.getAttribute("paintCeil").toString();

        int doors = (Integer) session.getAttribute("doors");
        String paintForDoors = session.getAttribute("paintDoors").toString();

        int windows = (Integer) session.getAttribute("windows");
        String paintForWindows = session.getAttribute("paintWindows").toString();


        System.out.println("Test paint for wall: " + paintForWall);

        //Jeg har ændret så den returnerer det id sql databasen laver, så spare vi noget kræft
        Task task = new Task(0, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now().plusWeeks(4)), total, newUserId);
        int newTaskId = td.createTask(task);
        task.setTaskId(newTaskId);

        pd.assignmentProducts(newTaskId, "wall", paintForWall, wall);
        pd.assignmentProducts(newTaskId, "ceil", paintForCeil, ceil);
        pd.assignmentProducts(newTaskId, "normalDoor", paintForDoors, doors);
        pd.assignmentProducts(newTaskId, "windowMedium", paintForWindows,  windows);
        session.setAttribute("username", (firstname + "-" + newUserId));
        request.setAttribute("task" , task);
        session.setAttribute("user", user);
        request.setAttribute("pword", password);
        
        request.getRequestDispatcher("loggedKunde.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
