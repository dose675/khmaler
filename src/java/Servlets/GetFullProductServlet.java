package Servlets;

import CRUD.ProductDao;
import CRUD.ProductDaoImpl;
import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import CRUD.UserDao;
import CRUD.UserDaoImpl;
import Entities.*;
import database.connection;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Responsible for retreiving data for a task, which includes data of the user
 * and the "products" what work needs to be done, and also generates a page and
 * displays all the data
 *
 * @author Arvid
 */
@WebServlet(name = "GetFullProductServlet", urlPatterns = {"/GetFullProductServlet"})
public class GetFullProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User userCheck = (User) session.getAttribute("user");
        int id = Integer.parseInt(request.getParameter("id"));
        TaskDao td = new TaskDaoImpl(connection.openConnection());
        Task task = td.getTaskById(id);
        if (null != userCheck) {
            if (userCheck.getId() != task.getCustommerId() && !(userCheck.getRole().toLowerCase().matches("maler"))) {
                session.invalidate();
                request.setAttribute("errormessage", "Invalid user info!");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        ProductDao pd = new ProductDaoImpl(connection.openConnection());
        List<Product> list = pd.getRelatedProducts(id);
        UserDao ud = new UserDaoImpl(connection.openConnection());
        User user = ud.getUserById(task.getCustommerId());
        List<User> painterList = td.getRelatedPainters(id);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GetFullProductServlet</title>");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "        <link href=\"1Årsprojekt.css\" rel=\"stylesheet\">");
            out.println("<style>/*lånt fra w3schools*/\n"
                    + "table {\n"
                    + "    font-family: arial, sans-serif;\n"
                    + "    border-collapse: collapse;\n"
                    + "    width: 100%;\n"
                    + "}\n"
                    + "\n"
                    + "td, th {\n"
                    + "    border: 1px solid #dddddd;\n"
                    + "    text-align: left;\n"
                    + "    padding: 8px;\n"
                    + "}\n"
                    + "tr:hover, tr:nth-child(even):hover{\n"
                    + "    background: #6699ff;\n"
                    + "    cursor: pointer;\n"
                    + "}\n"
                    + "tr:nth-child(even) {\n"
                    + "    background-color: #dddddd;\n"
                    + "}</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Alle Produkter tilhørende opgave: " + id + "</h1>");
            out.println("<p>Fornavn:    " + user.getFirstname() + "</p>");
            out.println("<p>Efternavn:  " + user.getLastname() + "</p>");
            out.println("<p>Adresse:    " + user.getAddress() + "</p>");
            out.println("<p>Postnummer: " + user.getZip() + "</p>");
            out.println("<p>By: " + user.getCity() + "</p>");
            out.println("<p>Bruger Id:  " + user.getId() + "</p>");
            out.println("<p>Telefon:    " + user.getPhone() + "</p>");
            out.println("<p>Email:      " + user.getEmail() + "</p>");
            out.println("<table>");
            out.println("<th>Produkt Navn</th>");
            out.println("<th>Mængde</th>");
            out.println("<th>Produkt Pris(stk)</th>");
            out.println("<th>Type(Maling?)</th>");
            for (Product products : list) {
                out.println("<tr>");
                out.println("<td>" + products.getProductName() + "</td>");
                out.println("<td>" + products.getAmount() + "</td>");
                out.println("<td>" + products.getPrice() + "</td>");
                out.println("<td>" + products.getPaint() + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("<p><b>Estimeret Pris: " + task.getTotalPrice() + "</b></p>");
            if(!painterList.isEmpty()){
                out.println("<p>Din maler/e: " + "</p>");
                for(User painters : painterList){
                    out.println("<p>Navn: " + painters.getFirstname() + " " + painters.getLastname() + "</p>");
                    out.println("<p>Telefon: " + painters.getPhone() + "</p>");
                    out.println("<p>Email: " + painters.getEmail() + "</p>");
                }
            }else{
                out.println("<p>Der endnu ikke tilknyttet nogen maler til din opgave</p>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
