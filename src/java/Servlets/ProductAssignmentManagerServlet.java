package Servlets;

import CRUD.ProductDao;
import CRUD.ProductDaoImpl;
import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import Entities.Product;
import Entities.User;
import database.connection;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles products related to a task.
 * Features: Create, delete, update
 * @author Arvid
 */
@WebServlet(name = "ProductAssignmentManager", urlPatterns = {"/ProductAssignmentManager"})
public class ProductAssignmentManagerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        String userid = (String) session.getAttribute("username");
        if(user != null && user.getRole().toLowerCase().equals("maler")){
            String input = request.getParameter("type");
            ProductDao pd = new ProductDaoImpl(connection.openConnection());
            int assignmentId = Integer.parseInt(request.getParameter("assignmentId"));
            String productName = request.getParameter("productName");
            String paintName = (request.getParameter("paintName"));
            Product product = new Product();
            product.setAssignmentId(assignmentId);
            product.setProductName(productName);
            switch(input){
                case "create":
                    int amount = Integer.parseInt(request.getParameter("productAmount"));
                    product.setAmount(amount);
                    product.setPaint(paintName);
                    System.out.println(product.getPaint() +  " " + product.getProductName() + " " + product.getAmount() + " " + product.getAssignmentId());
                    pd.addProductToTask(product);
                    break;
                case "update":
                    int amount1 = Integer.parseInt(request.getParameter("productAmount"));
                    product.setAmount(amount1);
                    product.setPaint(paintName);
                    System.out.println(product.getPaint() +  " " + product.getProductName() + " " + product.getAmount() + " " + product.getAssignmentId());
                    pd.updateTaskProduct(product);
                    break;
                case "delete":
                    pd.removeProductFromTask(productName, assignmentId);
                    break;
            }
        }
        session.setAttribute("username", userid);
        session.setAttribute("user", user);
        TaskDao td = new TaskDaoImpl(connection.openConnection());
        request.setAttribute("tasklist", td.findAll());
        request.getRequestDispatcher("/loggedMaler.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
