package Servlets;

import CRUD.ProductDao;
import CRUD.ProductDaoImpl;
import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import database.connection;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handling request related to Paint inventory management
 *
 * @author Arvid
 */
@WebServlet(name = "ProductServlet", urlPatterns = {"/ProductServlet"})
public class ProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("username");
        if (userid != null) {
            String input = request.getParameter("type");
            ProductDao pd = new ProductDaoImpl(connection.openConnection());
            switch (input) {
                case "delete":
                    deleteProduct(request, pd);
                    break;
                case "update":
                    updateProduct(request, pd);
                    break;
                case "create":
                    createProduct(request, pd);
                    break;
            }

        }
        TaskDao td = new TaskDaoImpl(connection.openConnection());
        request.setAttribute("tasklist", td.findAll());
        request.getRequestDispatcher("/loggedMaler.jsp").forward(request, response);
    }

    /**
     * Retreives the id to be deleted, and forwards it to ProductDao to be
     * deleted
     *
     * @param request serlvet request
     * @param pd ProductDao interface to connet to DB
     */
    private void deleteProduct(HttpServletRequest request, ProductDao pd) {
        try {
            String paintName = request.getParameter("id");
            //int id = Integer.parseInt(request.getParameter("id"));
            pd.deleteProduct(paintName);
        } catch (NumberFormatException e) {
        }
    }

    /**
     * Handles getting parameters for what to update, and check if checkboxes
     * are check, if so = 1 and then forwarding it to the ProductDao to be
     * updated
     *
     * @param request servlet request
     * @param pd ProductDao interface to connet to DB
     */
    private void updateProduct(HttpServletRequest request, ProductDao pd) {
        try {
            //int id = Integer.parseInt(request.getParameter("id"));
            String id = request.getParameter("id");
            String name = request.getParameter("paintName");
            int ceil = request.getParameter("ceil") != null ? 1 : 0;
            int wall = request.getParameter("wall") != null ? 1 : 0;
            int door = request.getParameter("door") != null ? 1 : 0;
            int window = request.getParameter("window") != null ? 1 : 0;
            pd.updateProduct(id, name, ceil, wall, door, window);
        } catch (NumberFormatException e) {
        }
    }

    /**
     * Handles getting parameters for what to create, and check if checkboxes
     * are check, if so = 1 and then forwarding it to the ProductDao to be
     * created
     *
     * @param request servlet request
     * @param pd ProductDao interface to connet to DB
     */
    private void createProduct(HttpServletRequest request, ProductDao pd) {
        String name = request.getParameter("name");
        System.out.println(request.getParameter("ceil") + " " + request.getParameter("wall"));
        int ceil = request.getParameter("ceil") != null ? 1 : 0;
        int wall = request.getParameter("wall") != null ? 1 : 0;
        int door = request.getParameter("door") != null ? 1 : 0;
        int window = request.getParameter("window") != null ? 1 : 0;
        pd.createProduct(name, ceil, wall, door, window);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
