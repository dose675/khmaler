package Servlets;

import CRUD.UserDao;
import CRUD.UserDaoImpl;
import Entities.User;
import database.connection;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * For viewing the entire userlist retreives all users from the database and
 * generates the page itself
 * @author Arvid
 */
@WebServlet(name = "ViewUsersServlet", urlPatterns = {"/ViewUsersServlet"})
public class ViewUsersServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserDao ud = new UserDaoImpl(connection.openConnection());
        User user = (User) session.getAttribute("user");
        if (user != null) {
            if (session.getAttribute("username") == null && user.getRole().toLowerCase().equals("maler")) {
                session.invalidate();
                request.setAttribute("errormessage", "Invalid user info!");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        }else{
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        List<User> list = ud.findAll();
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewUsersServlet</title>");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "        <link href=\"1Årsprojekt.css\" rel=\"stylesheet\">");
            out.println("<style>/*lånt fra w3schools*/\n"
                    + "table {\n"
                    + "    font-family: arial, sans-serif;\n"
                    + "    border-collapse: collapse;\n"
                    + "    width: 100%;\n"
                    + "}\n"
                    + "\n"
                    + "td, th {\n"
                    + "    border: 1px solid #dddddd;\n"
                    + "    text-align: left;\n"
                    + "    padding: 8px;\n"
                    + "}\n"
                    + "tr:hover, tr:nth-child(even):hover{\n"
                    + "    background: #6699ff;\n"
                    + "    cursor: pointer;\n"
                    + "}\n"
                    + "tr:nth-child(even) {\n"
                    + "    background-color: #dddddd;\n"
                    + "}</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Bruger Liste</h1>");
            out.println("<table>");
            out.println("<th>Bruger id</th>");
            out.println("<th>Fornavn</th>");
            out.println("<th>Efternavn</th>");
            out.println("<th>Adresse</th>");
            out.println("<th>Postnummer</th>");
            out.println("<th>By</th>");
            out.println("<th>Telefon Nummer</th>");
            out.println("<th>Email</th>");
            out.println("<th>Rolle</th>");
            for (User users : list) {
                out.println("<tr>");
                out.println("<td>" + users.getId() + "</td>");
                out.println("<td>" + users.getFirstname() + "</td>");
                out.println("<td>" + users.getLastname() + "</td>");
                out.println("<td>" + users.getAddress() + "</td>");
                out.println("<td>" + users.getZip() + "</td>");
                out.println("<td>" + users.getCity() + "</td>");
                out.println("<td>" + users.getPhone() + "</td>");
                out.println("<td>" + users.getEmail() + "</td>");
                out.println("<td>" + users.getRole() + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
