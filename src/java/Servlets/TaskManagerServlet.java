package Servlets;

import CRUD.TaskDao;
import CRUD.TaskDaoImpl;
import Entities.Task;
import database.connection;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles task management, and painters assigned to them
 * Features: Create, delete, update, assign and remove
 * @author Arvid
 */
@WebServlet(name = "TaskManagerServlet", urlPatterns = {"/TaskManager"})
public class TaskManagerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("username");
        if (userid != null) {
            TaskDao td = new TaskDaoImpl(connection.openConnection());
            String input = request.getParameter("type");
            switch (input) {
                case "create":
                    td.createManualTask(create(request));
                    break;
                case "delete":
                    td.deleteTask(Integer.parseInt(request.getParameter("delete")));
                    request.setAttribute("dMessage", "Delete");
                    break;
                case "update":
                    td.updateTask(updater(request, td));
                    break;
                case "assign":
                    assignPainter(request, td);
                    break;
                case "remove":
                    removePainter(request, td);
                    break;
            }
            request.setAttribute("tasklist", td.findAll());
            request.getRequestDispatcher("/loggedMaler.jsp").forward(request, response);
        } else {
            session.invalidate();
            request.setAttribute("errorMessage", "Session Timed out/Not Available!");
            request.getRequestDispatcher("/Login.jsp").forward(request, response);
        }

    }

    /**
     * Retrieves parameters from form, constructs a Task object from them and
     * returns it For it to be sent further on to be updated
     *
     * @param request servlet request
     * @return Task (To be updated)
     */
    private Task updater(HttpServletRequest request, TaskDao td) {
        //Task task = null; 
        int assignmentId = Integer.parseInt(request.getParameter("assignmentId"));
        Task oldTask = td.getTaskById(assignmentId);
        //Ternary operators, easier than if statements: 
        LocalDate upDateCr = !"".equals(request.getParameter("upDateCr"))
                ? LocalDate.parse(request.getParameter("upDateCr")) : oldTask.getDateOfCreation().toLocalDate();
        LocalDate upDateJob = !"".equals(request.getParameter("upDateJob"))
                ? LocalDate.parse(request.getParameter("upDateJob")) : oldTask.getDateOfJob().toLocalDate();
        double uTotal = !"".equals(request.getParameter("upTotal"))
                ? Double.parseDouble(request.getParameter("upTotal")) : oldTask.getTotalPrice();
        int upCustommerId = !"".equals(request.getParameter("upCustommer"))
                ? Integer.parseInt(request.getParameter("upCustommer")) : oldTask.getCustommerId();

        Task task = new Task(assignmentId, Date.valueOf(upDateCr), Date.valueOf(upDateJob), uTotal, upCustommerId);
        request.setAttribute("uMessage", "Opgave opdateret!");
        return task;
    }

    /**
     * Retrieves parameters from a form, constructs a task object from tme and
     * returns it For it to be sent further on to be created
     *
     * @param request servlet request
     * @return Task (To be created)
     */
    private Task create(HttpServletRequest request) {
        Task task = null;
        LocalDate createDateCr = !"".equals(request.getParameter("createDateCr"))
                ? LocalDate.parse(request.getParameter("createDateCr")) : LocalDate.now();
        LocalDate createDateJob = !"".equals(request.getParameter("createDateJob"))
                ? LocalDate.parse(request.getParameter("createDateJob")) : LocalDate.now().plusWeeks(4);
        double total = Double.parseDouble(request.getParameter("createPriceTotal"));
        int custommerId = Integer.parseInt(request.getParameter("createCustommerId"));
        task = new Task(0, Date.valueOf(createDateCr), Date.valueOf(createDateJob), total, custommerId);
        request.setAttribute("cMessage", "Opgave oprettet!");
        return task;
    }

    /**
     * Handles adding painter to assignment request
     *
     * @param request servlet request
     * @param td TaskDao interface, to assign painter to task in DB
     */
    private void assignPainter(HttpServletRequest request, TaskDao td) {
        int taskId = Integer.parseInt(request.getParameter("TskId"));
        int painterId = Integer.parseInt(request.getParameter("painterId"));
        td.assignPainter(taskId, painterId);
    }

    /**
     * Handles remove painter from assignment requests
     *
     * @param request servlet request
     * @param td TaskDao interface to remove painter from task in DB
     */
    private void removePainter(HttpServletRequest request, TaskDao td) {
        int taskId = Integer.parseInt(request.getParameter("TskId"));
        int painterId = Integer.parseInt(request.getParameter("painterId"));
        td.removePainter(taskId, painterId);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
